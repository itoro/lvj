#pragma once

#include <poll.h>

#include "error.hpp"
#include "meta.hpp"
#include "srm.hpp"

struct fd_helper_t {
  static const int invalid;
  static bool is_valid(int fd);
  static void reset(int fd);
};

using fd_t = srm<int, fd_helper_t>;

void s_close(int fd);

fd_t s_open(const char *pathname, int flags);

size_t s_write(int fd, const void *buf, size_t count);

int s_poll(struct pollfd *fds, nfds_t nfds, int timeout);

size_t s_read(int fd, void *buf, size_t count);

inline auto full_write(const auto &fd, const auto &data) {
  constexpr auto sz = sizeof(data);
  auto w = AS(sz, 0);
  auto buf = pointer_cast_f<char>(&data);
  static_assert(std::is_same_v<decltype(buf), const char *>);
  while (w < sz) {
    w += s_write(fd, &buf[w], sz - w);
  }
  check(POS, w == sz, "full_write failed");
}

inline auto full_read(const auto &fd, auto &data) {
  constexpr auto sz = sizeof(data);
  auto r = AS(sz, 0);
  auto buf = pointer_cast_f<char>(&data);
  static_assert(std::is_same_v<decltype(buf), char *>);
  while (r < sz) {
    r += s_read(fd, &buf[r], sz - r);
  }
  check(POS, r == sz, "full_read failed");
  return data;
}
