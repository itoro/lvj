#pragma once

#include <utility>

template<class T, class Hlp>
class srm {
 public:
  using type = srm<T, Hlp>;
  srm() : _sr{Hlp::invalid} {}
  srm(T sr) : srm{} { this->take(sr); }
  srm(type &&other) : srm{} { *this = std::move(other); }
  ~srm() {
    try {
      this->reset();
    } catch (...) {
    }
  }
  type &operator=(type &&other) {
    this->take(other.release());
    return *this;
  }
  auto &get() const { return this->_sr; }
  auto release() { return std::exchange(this->_sr, Hlp::invalid); }
  auto reset() {
    auto sr = this->release();
    if (Hlp::is_valid(sr)) {
      Hlp::reset(sr);
    }
  }
  auto take(T sr) {
    this->reset();
    this->_sr = sr;
  }
  srm(const type &) = delete;
  type &operator=(const type &) = delete;

 private:
  T _sr;
};
