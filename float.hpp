#pragma once

#include <cmath>

#include <iostream>

#ifdef USEDOUBLE
using _support_t = double;
#else
using _support_t = float;
#endif

class flt_t {
 public:
  flt_t() = default;
  constexpr flt_t(int n) : _value{(_support_t)n} {}
  constexpr flt_t(int num, int denom) :
      _value{(_support_t)num / (_support_t)denom} {}
  explicit flt_t(_support_t x) : _value{x} {}
  template<class T>
  explicit operator T() const {
    return (T)this->_value;
  }
  flt_t &operator/=(flt_t k) {
    this->_value /= k._value;
    return *this;
  }
  _support_t _get() const { return this->_value; }

 private:
  _support_t _value;
};

inline auto operator<=>(const flt_t &x, const flt_t &y) {
  return x._get() <=> y._get();
}

inline auto operator==(const flt_t &x, const flt_t &y) {
  return x._get() == y._get();
}

inline auto operator!=(const flt_t &x, const flt_t &y) {
  return x._get() != y._get();
}

inline auto operator<=(const flt_t &x, const flt_t &y) {
  return x._get() <= y._get();
}

inline auto operator<(const flt_t &x, const flt_t &y) {
  return x._get() < y._get();
}

inline auto operator>=(const flt_t &x, const flt_t &y) {
  return x._get() >= y._get();
}

inline auto operator>(const flt_t &x, const flt_t &y) {
  return x._get() > y._get();
}

inline auto operator+(const flt_t &x, const flt_t &y) {
  return flt_t{x._get() + y._get()};
}

inline auto operator-(const flt_t &x, const flt_t &y) {
  return flt_t{x._get() - y._get()};
}

inline auto operator-(const flt_t &x) { return flt_t{-x._get()}; }

inline auto operator*(const flt_t &x, const flt_t &y) {
  return flt_t{x._get() * y._get()};
}

inline auto operator/(const flt_t &x, const flt_t &y) {
  return flt_t{x._get() / y._get()};
}

inline auto &operator<<(std::ostream &os, const flt_t &x) {
  return os << x._get();
}

namespace std {
  inline auto round(const flt_t &x) {
    return flt_t{std::round(x._get())};
  }
}
