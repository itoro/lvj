#pragma once

#include "data.hpp"
#include "error.hpp"

inline auto mk_dz(
    const auto &min,
    const auto &ldz,
    const auto &rdz,
    const auto &max) {
  assert(POS, min <= ldz);
  assert(POS, ldz <= rdz);
  assert(POS, rdz <= max);
  auto amp = std::min(ldz - min, max - rdz);
  return table_t<value_t>{
      .x = {ldz - amp, ldz, rdz, rdz + amp}, .y = {-1, 0, 0, 1}};
}

inline auto mk_sym_abs_output(const auto &unit) {
  return abs_output_t{
      .min = -unit, .max = unit, .zero = 0, .unit = unit};
}

inline auto mk_half_abs_output(const auto &unit) {
  return abs_output_t{.min = 0, .max = unit, .zero = 0, .unit = unit};
}

inline auto make_anti_dz(
    const auto &max, const auto &sat, const auto &dz) {
  auto x0 = flt_t{1, 2 * (sat - dz)};
  auto y0 = dz + flt_t{1, 2};
  return table_t<flt_t>{
      .x = {-1, -x0, x0, 1},
      .y = {-flt_t{sat, max}, -y0 / max, y0 / max, flt_t{sat, max}}};
}

inline const auto modern_keys = std::set<code_t>{
    BTN_SOUTH,
    BTN_EAST,
    BTN_NORTH,
    BTN_WEST,
    BTN_TL,
    BTN_TR,
    BTN_SELECT,
    BTN_START,
    BTN_MODE,
    BTN_THUMBL,
    BTN_THUMBR};

inline const auto hat_abs_output = mk_sym_abs_output(1);

inline const auto trigger_abs_output = mk_half_abs_output(0x7fff);

inline const auto acc_abs_output = mk_sym_abs_output(0x8000);

inline const auto gyr_abs_output = mk_sym_abs_output(0x200000);

inline const auto keyboard_keys = range_set<code_t, 1U, 128U>::f();

namespace lvj {

  inline const auto ds4 = output_t{
      .name = "Linux Virtual DS4",
      .id = {.bustype = BUS_USB, .vendor = 0x3fc2, .product = 0x4f92},
      .keys = modern_keys,
      .abss = {
          {0U, {}},
          {1U, {}},
          {2U, {trigger_abs_output}},
          {3U, {}},
          {4U, {}},
          {5U, {trigger_abs_output}},
          {ABS_HAT0X, {hat_abs_output}},
          {ABS_HAT0Y, {hat_abs_output}}}};

  inline const auto full_ds4 = output_t{
      .name = "Linux Virtual Full DS4",
      .id = {.bustype = BUS_USB, .vendor = 0x3fc2, .product = 0x53da},
      .keys = modern_keys,
      .abss = {
          {0U, {}},
          {1U, {}},
          {2U, {trigger_abs_output}},
          {3U, {}},
          {4U, {}},
          {5U, {trigger_abs_output}},
          {6U, {acc_abs_output}},
          {7U, {acc_abs_output}},
          {8U, {acc_abs_output}},
          {9U, {gyr_abs_output}},
          {10U, {gyr_abs_output}},
          {11U, {gyr_abs_output}},
          {ABS_HAT0X, {hat_abs_output}},
          {ABS_HAT0Y, {hat_abs_output}}}};

  inline const auto sixaxis = output_t{
      .name = "Linux Virtual Sixaxis",
      .id = {.bustype = BUS_USB, .vendor = 0x3fc2, .product = 0xd828},
      .keys = {BTN_0},
      .abss = {
          {0U, {}}, {1U, {}}, {2U, {}}, {3U, {}}, {4U, {}}, {5U, {}}}};

  inline const auto simple_joystick = output_t{
      .name = "Linux Virtual Joystick",
      .id = {.bustype = BUS_USB, .vendor = 0x3fc2, .product = 0xcb9d},
      .keys = {BTN_0},
      .abss = {{0U, {}}, {1U, {}}}};

  inline const auto snes = output_t{
      .name = "Linux Virtual SNES Pad",
      .id = {.bustype = BUS_USB, .vendor = 0x3fc2, .product = 0xb8b7},
      .keys =
          {BTN_SOUTH,
           BTN_EAST,
           BTN_NORTH,
           BTN_WEST,
           BTN_TL,
           BTN_TR,
           BTN_SELECT,
           BTN_START},
      .abss = {
          {ABS_HAT0X, {hat_abs_output}},
          {ABS_HAT0Y, {hat_abs_output}}}};

  inline const auto generic_keyboard = output_t{
      .name = "Linux Virtual Generic Keyboard",
      .id = {.bustype = BUS_USB, .vendor = 0x3fc2, .product = 0xe4a5},
      .keys = keyboard_keys};

  inline constexpr auto vr_key_shift_down = BTN_0;
  inline constexpr auto vr_key_shift_up = BTN_1;
  inline constexpr auto vr_key_coin = BTN_2;
  inline constexpr auto vr_key_start = BTN_3;

  inline constexpr auto vr_abs_wheel = 0U;
  inline constexpr auto vr_abs_accelerator = 1U;
  inline constexpr auto vr_abs_brake = 2U;
  inline constexpr auto vr_abs_yellow_green = ABS_HAT0X;
  inline constexpr auto vr_abs_red_blue = ABS_HAT0Y;

  inline const auto vr = output_t{
      .name = "Linux Virtual MAME Virtua Racing Controller",
      .id = {.bustype = BUS_USB, .vendor = 0x3fc2, .product = 0xcefd},
      .keys =
          {vr_key_shift_down,
           vr_key_shift_up,
           vr_key_coin,
           vr_key_start},
      .abss = {
          {vr_abs_wheel, {}},
          {vr_abs_accelerator, {}},
          {vr_abs_brake, {}},
          {vr_abs_yellow_green, {}},
          {vr_abs_red_blue, {}}}};

  namespace xbox {

    inline const auto xbox_360 = output_t{
        .name = "Xbox 360 Controller",
        .id =
            {.bustype = BUS_USB,
             .vendor = 0x045e,
             .product = 0x028e,
             .version = 0x0114},
        .keys = modern_keys,
        .abss = {
            {0U, {}},
            {1U, {}},
            {2U, {trigger_abs_output}},
            {3U, {}},
            {4U, {}},
            {5U, {trigger_abs_output}},
            {ABS_HAT0X, {hat_abs_output}},
            {ABS_HAT0Y, {hat_abs_output}}}};

  }

}

namespace sony {

  inline const auto raw_stick_input_table =
      table_t<value_t>{.x = {1, 255}, .y = {-1, 1}};

  inline const auto trigger_input_table =
      table_t<value_t>{.x = {0, 255}, .y = {0, 1}};

  inline const auto raw_ds4 = input_t{
      .keys = modern_keys,
      .abss = {
          {0U, raw_stick_input_table},
          {1U, raw_stick_input_table},
          {2U, trigger_input_table},
          {3U, raw_stick_input_table},
          {4U, raw_stick_input_table},
          {5U, trigger_input_table}}};

  inline const auto new_black = input_t{
      .abss = {
          {0U, mk_dz(1, 123, 132, 255)},
          {1U, mk_dz(0, 113, 134, 255)},
          {2U, trigger_input_table},
          {3U, mk_dz(0, 119, 124, 255)},
          {4U, mk_dz(0, 122, 128, 255)},
          {5U, trigger_input_table}}};

  inline const auto new_new_black = input_t{
      .keys = modern_keys,
      .abss = {
          {0U, mk_dz(0, 125, 132, 255)},
          {1U, mk_dz(0, 125, 130, 255)},
          {2U, trigger_input_table},
          {3U, mk_dz(0, 124, 130, 255)},
          {4U, mk_dz(0, 124, 129, 255)},
          {5U, trigger_input_table}}};

  inline const auto sixaxis = input_t{};

  inline const auto sixaxis_to_full_ds4_mapping = mapping_t{
      .abss =
          {{0U, {.code = 6U}},
           {1U, {.code = 7U}},
           {2U, {.code = 8U}},
           {3U, {.code = 9U}},
           {4U, {.code = 10U}},
           {5U, {.code = 11U}}},
      .keep_other_abss = false};

}

namespace logitech {

  inline const auto g29 = input_t{
      .abss = {
          {{0U, {.x = {1, 65535}, .y = {-1, 1}}},
           {1U, {.x = {0, 255}, .y = {1, 0}}},
           {2U, {.x = {0, 255}, .y = {1, 0}}},
           {5U, {.x = {100, 255}, .y = {1, 0}}}}}};

}

namespace thrustmaster {

  inline const auto hotas_4_input = input_t{
      .abss =
          {{0U, mk_dz(0, 512, 512, 1023)},
           {1U, mk_dz(0, 512, 512, 1023)},
           {2U, mk_dz(0, 128, 128, 255)},
           {5U, mk_dz(0, 128, 128, 255)},
           {7U, mk_dz(0, 128, 128, 255)},
           {16U, mk_dz(-1, 0, 0, 1)},
           {17U, mk_dz(-1, 0, 0, 1)}},
      .keep_other_abss = false};

  inline const auto hotas_4_output = output_t{
      .name = "Linux Virtual T.Flight Hotas 4",
      .id = {.bustype = BUS_USB, .vendor = 0x3fc2, .product = 0x919d},
      .keys = range_set<code_t, 288U, 300U>::f(),
      .abss = {
          {ABS_X, {}},
          {ABS_Y, {}},
          {ABS_Z, {}},
          {ABS_THROTTLE, {}},
          {ABS_RUDDER, {}},
          {ABS_HAT0X, {}},
          {ABS_HAT0Y, {}},
      }};

  inline const auto hotas_4_mapping = mapping_t{
      .keep_other_keys = true,
      .abss =
          {{0U, {.code = ABS_X}},
           {1U, {.code = ABS_Y}},
           {2U, {.code = ABS_THROTTLE}},
           {5U, {.code = ABS_Z}},
           {7U, {.code = ABS_RUDDER}}},
      .keep_other_abss = true};

  inline const auto hotas_4_config = config_t{
      .name = "T.Flight Hotas 4",
      .inputs = {hotas_4_input},
      .outputs = {hotas_4_output},
      .mappings =
          {{0U, {{.controller = 1U, .mapping = hotas_4_mapping}}}},
      .output_mappings = {{1U, 0U}}};

}

namespace mame {

  inline constexpr auto vr_w_max = 127;
  inline constexpr auto vr_w_sat = 83;
  inline constexpr auto vr_w_dz = 5;

  inline const auto vr_mapping = mapping_t{
      .keep_other_keys = true,
      .abss =
          {{lvj::vr_abs_wheel,
            {.table = make_anti_dz(vr_w_max, vr_w_sat, vr_w_dz)}},
           {lvj::vr_abs_accelerator,
            {.table =
                 {{.x = {0, 1},
                   .y = {0, (flt_t{0xcd - 0x30, 0xff - 0x30})}}}}},
           {lvj::vr_abs_brake,
            {.table =
                 {{.x = {0, 1},
                   .y = {0, (flt_t{0xa7 - 0x30, 0xff - 0x30})}}}}},
           {lvj::vr_abs_yellow_green, {}},
           {lvj::vr_abs_red_blue, {}}},
      .keep_other_abss = false};

  inline const auto ds4_to_vr_mapping = mapping_t{
      .keys =
          {{BTN_SOUTH, lvj::vr_key_shift_up},
           {BTN_WEST, lvj::vr_key_shift_down},
           {BTN_SELECT, lvj::vr_key_coin},
           {BTN_START, lvj::vr_key_start}},
      .keep_other_keys = false,
      .abss =
          {{0U, {.code = lvj::vr_abs_wheel}},
           {2U, {.code = lvj::vr_abs_brake}},
           {5U, {.code = lvj::vr_abs_accelerator}},
           {ABS_HAT0X, {.code = lvj::vr_abs_yellow_green}},
           {ABS_HAT0Y, {.code = lvj::vr_abs_red_blue}}},
      .keep_other_abss = false};

  inline const auto g29_to_vr_mapping = mapping_t{
      .keys =
          {{293U, lvj::vr_key_shift_down},
           {292U, lvj::vr_key_shift_up},
           {295U, lvj::vr_key_coin},
           {294U, lvj::vr_key_start}},
      .keep_other_keys = false,
      .abss =
          {{0U,
            {.code = lvj::vr_abs_wheel,
             .table =
                 {{.x = {flt_t{-180, 370}, flt_t{180, 370}},
                   .y = {-1, 1}}}}},
           {2U, {.code = lvj::vr_abs_accelerator}},
           {5U, {.code = lvj::vr_abs_brake}},
           {ABS_HAT0X, {.code = lvj::vr_abs_yellow_green}},
           {ABS_HAT0Y, {.code = lvj::vr_abs_red_blue}}},
      .keep_other_abss = false};

  inline const auto new_black_vr = config_t{
      .name = "Virtua Racing New Black",
      .inputs = {sony::new_black},
      .outputs = {lvj::vr},
      .mappings =
          {{0U,
            {{.controller = 1U, .mapping = ::mame::ds4_to_vr_mapping}}},
           {1U, {{.controller = 2U, .mapping = ::mame::vr_mapping}}}},
      .output_mappings = {{2U, 0U}}};

  inline const auto new_new_black_vr = config_t{
      .name = "Virtua Racing New New Black",
      .inputs = {sony::new_new_black},
      .outputs = {lvj::vr},
      .mappings =
          {{0U,
            {{.controller = 1U, .mapping = mame::ds4_to_vr_mapping}}},
           {1U, {{.controller = 2U, .mapping = mame::vr_mapping}}}},
      .output_mappings = {{2U, 0U}}};

  inline const auto g29_vr = config_t{
      .name = "Virtua Racing G29",
      .inputs = {logitech::g29},
      .outputs = {lvj::vr},
      .mappings =
          {{0U, {{.controller = 1U, .mapping = g29_to_vr_mapping}}},
           {1U, {{.controller = 2U, .mapping = vr_mapping}}}},
      .output_mappings = {{2U, 0U}}};

}

inline const auto snes_kbd_input = input_t{
    .keys =
        {{KEY_W,
          KEY_A,
          KEY_S,
          KEY_D,
          KEY_J,
          KEY_K,
          KEY_L,
          KEY_U,
          KEY_I,
          KEY_O,
          KEY_BACKSPACE,
          KEY_ENTER}},
    .keep_other_abss = false,
};

inline const auto kbd_to_snes_mapping = mapping_t{
    .keys =
        {
            {KEY_J, BTN_SOUTH},
            {KEY_K, BTN_EAST},
            {KEY_L, BTN_TR},
            {KEY_U, BTN_WEST},
            {KEY_I, BTN_NORTH},
            {KEY_O, BTN_TL},
            {KEY_BACKSPACE, BTN_SELECT},
            {KEY_ENTER, BTN_START},
        },
    .keep_other_keys = false,
    .keep_other_abss = false,
    .keys_to_abss = {
        {KEY_W, {.code = ABS_HAT0Y, .value = -1}},
        {KEY_A, {.code = ABS_HAT0X, .value = -1}},
        {KEY_S, {.code = ABS_HAT0Y, .value = 1}},
        {KEY_D, {.code = ABS_HAT0X, .value = 1}}}};

inline const auto xbox_mapping = mapping_t{
    .keys = {{BTN_WEST, {BTN_NORTH}}, {BTN_NORTH, {BTN_WEST}}},
    .keep_other_keys = true,
    .keep_other_abss = true};

inline const auto xbox_driving_mapping = mapping_t{
    .keys =
        {{BTN_SOUTH, {BTN_TR}},
         {BTN_NORTH, {BTN_WEST}},
         {BTN_WEST, {BTN_TL}},
         {BTN_TL, {BTN_NORTH}},
         {BTN_TR, {BTN_SOUTH}}},
    .keep_other_keys = true,
    .keep_other_abss = true};

inline const auto ds4_to_keyboard_mapping = mapping_t{
    .keys =
        {{BTN_SOUTH, {KEY_ENTER}},
         {BTN_EAST, {KEY_ESC}},
         {BTN_NORTH, {KEY_UP}},
         {BTN_WEST, {KEY_DOWN}},
         {BTN_TL, {KEY_LEFT}},
         {BTN_TR, {KEY_RIGHT}}},
    .keep_other_keys = false};

inline const auto ds4_to_flatout_controller_mapping = mapping_t{
    .keys =
        {{BTN_NORTH, {BTN_WEST}},
         {BTN_SOUTH, {}},
         {BTN_WEST, {BTN_NORTH}},
         {BTN_EAST, {}},
         {BTN_TL, {}},
         {BTN_TR, {}},
         {BTN_START, {}},
         {BTN_SELECT, {}}},
    .keep_other_keys = false,
    .abss = {{0U, {}}, {2U, {}}, {5U, {}}},
    .keep_other_abss = false};

inline const auto ds4_to_flatout_keyboard_mapping = mapping_t{
    .keys =
        {{BTN_THUMBR, {KEY_ENTER}},
         {BTN_THUMBL, {KEY_ESC}},
         {BTN_MODE, {KEY_DELETE}}},
    .keep_other_keys = false,
    .keep_other_abss = false,
    .abss_to_keys = {
        {ABS_HAT0X,
         {{.code = {KEY_LEFT}, .left = -1, .right = {-2, 10}},
          {.code = {}, .left = {-3, 10}, .right = {3, 10}},
          {.code = {KEY_RIGHT}, .left = {2, 10}, .right = 1}}},
        {ABS_HAT0Y,
         {{.code = {KEY_UP}, .left = -1, .right = {-2, 10}},
          {.code = {}, .left = {-3, 10}, .right = {3, 10}},
          {.code = {KEY_DOWN}, .left = {2, 10}, .right = 1}}}}};

inline const auto pop_mapping = mapping_t{
    .keys = {{BTN_WEST, {KEY_LEFTSHIFT}}, {BTN_SOUTH, {KEY_LEFTCTRL}}},
    .keep_other_keys = false,
    .keep_other_abss = false,
    .abss_to_keys = {
        {ABS_HAT0X,
         {{.code = {KEY_LEFT}, .left = -1, .right = {-2, 10}},
          {.code = {}, .left = {-3, 10}, .right = {3, 10}},
          {.code = {KEY_RIGHT}, .left = {2, 10}, .right = 1}}},
        {ABS_HAT0Y,
         {{.code = {KEY_UP}, .left = -1, .right = {-2, 10}},
          {.code = {}, .left = {-3, 10}, .right = {3, 10}},
          {.code = {KEY_DOWN}, .left = {2, 10}, .right = 1}}}}};

namespace configs {

  inline const auto new_black = config_t{
      .name = "New Black Config",
      .inputs = {sony::new_black},
      .outputs = {lvj::ds4},
      .output_mappings = {{0U, 0U}},
      .ff_mappings = {{.input_controller = 0, .output_controller = 0}}};

  inline const auto new_new_black = config_t{
      .name = "New New Black Config",
      .inputs = {sony::new_new_black},
      .outputs = {lvj::ds4},
      .output_mappings = {{0, 0}},
      .ff_mappings = {{.input_controller = 0, .output_controller = 0}}};

  inline const auto new_black_sixaxis = config_t{
      .name = "Full DS4",
      .inputs = {sony::raw_ds4, sony::sixaxis},
      .outputs = {lvj::full_ds4},
      .mappings =
          {{1U,
            {{.controller = 2U,
              .mapping = sony::sixaxis_to_full_ds4_mapping}}}},
      .output_mappings = {{0U, 0U}, {2U, 0U}},
      .ff_mappings = {{0U, 0U}}};

  inline const auto xbox = config_t{
      .name = "XBox 360 Simulator",
      .inputs = {sony::new_new_black},
      .outputs = {lvj::xbox::xbox_360},
      .mappings = {{0U, {{.controller = 1U, .mapping = xbox_mapping}}}},
      .output_mappings = {{1U, 0U}},
      .ff_mappings = {
          {.input_controller = 0U, .output_controller = 0U}}};

  inline const auto xbox_driving = config_t{
      .name = "XBox 360 for Driving",
      .inputs = {sony::new_new_black},
      .outputs = {lvj::xbox::xbox_360},
      .mappings =
          {{0U, {{.controller = 1U, .mapping = xbox_driving_mapping}}}},
      .output_mappings = {{1U, 0U}},
      .ff_mappings = {
          {.input_controller = 0U, .output_controller = 0U}}};

  inline const auto raw_xbox = config_t{
      .name = "Raw XBox 360 Simulator",
      .inputs = {sony::raw_ds4},
      .outputs = {lvj::xbox::xbox_360},
      .mappings = {{0U, {{.controller = 1U, .mapping = xbox_mapping}}}},
      .output_mappings = {{1U, 0U}},
      .ff_mappings = {
          {.input_controller = 0U, .output_controller = 0U}}};

  inline const auto pop = config_t{
      .name = "Prince of Persia",
      .inputs = {sony::raw_ds4},
      .outputs = {lvj::generic_keyboard},
      .mappings = {{0U, {{.controller = 1U, .mapping = pop_mapping}}}},
      .output_mappings = {{1U, 0U}}};

  inline const auto flatout = config_t{
      .name = "FlatOut 2",
      .inputs = {sony::new_new_black},
      .outputs = {lvj::xbox::xbox_360, lvj::generic_keyboard},
      .mappings =
          {{0U,
            {
                {.controller = 1,
                 .mapping = ds4_to_flatout_controller_mapping},
                {.controller = 2,
                 .mapping = ds4_to_flatout_keyboard_mapping},
            }}},
      .output_mappings = {{1, 0}, {2, 1}},
      .ff_mappings = {{.input_controller = 0, .output_controller = 0}}};

  inline const auto kbd_to_snes = config_t{
      .name = "Keyboard to Virtual SNES",
      .inputs = {snes_kbd_input},
      .outputs = {lvj::snes},
      .mappings =
          {{0U, {{.controller = 1U, .mapping = kbd_to_snes_mapping}}}},
      .output_mappings = {{1U, 0U}}};

  inline const auto motion = config_t{
      .name = "Sixaxis Virtual Joystick",
      .inputs = {sony::sixaxis},
      .outputs = {lvj::sixaxis},
      .output_mappings = {{0U, 0U}}};

}
