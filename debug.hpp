#pragma once

#include <iostream>

#ifdef DEBUG
inline constexpr auto _debug = true;
#else
inline constexpr auto _debug = false;
#endif

inline auto _debug_level = 0;

inline auto set_debug_level(const auto &lvl) {
  // todo: how to use constexpr here?
  if (_debug) {
    _debug_level = lvl;
  }
}

inline auto fdebug_with(const auto &lvl, const auto &f) {
  // todo: how to use constexpr here?
  if (_debug) {
    if (_debug_level < 0 || _debug_level >= lvl) {
      auto fline = [](const auto &ff) {
        std::cerr << "# ";
        ff(std::cerr);
        std::cerr << '\n';
      };
      f(fline);
      std::cerr << std::flush;
    }
  }
}

inline auto fdebug(const auto &lvl, const auto &f) {
  fdebug_with(lvl, [&](const auto &fline) { fline(f); });
}
