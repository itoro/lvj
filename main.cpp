#include <cstring>

#include <algorithm>
#include <queue>
#include <unordered_map>

#include <fcntl.h>

#include "configs.hpp"
#include "debug.hpp"
#include "unix.hpp"

struct esc_type {
  const type_t &value;
};

struct esc_code {
  const code_t &value;
};

static const auto type_names = std::unordered_map<type_t, const char *>{
    {EV_SYN, "EV_SYN"},
    {EV_KEY, "EV_KEY"},
    {EV_REL, "EV_REL"},
    {EV_ABS, "EV_ABS"},
    {EV_MSC, "EV_MSC"},
    {EV_SW, "EV_SW"},
    {EV_LED, "EV_LED"},
    {EV_SND, "EV_SND"},
    {EV_REP, "EV_REP"},
    {EV_FF, "EV_FF"},
    {EV_PWR, "EV_PWR"},
    {EV_FF_STATUS, "EV_FF_STATUS"},
    {EV_UINPUT, "EV_UINPUT"}};

static auto find_opt(const auto &map, const auto &key) {
  using T = RAWTYPE(map.at(key));
  auto it = map.find(key);
  if (it == map.end()) {
    return option_t<const T, true>{};
  } else {
    return option_t<const T, true>{it->second};
  }
}

static const auto &find(
    const auto &fpos, const auto &map, const auto &key) {
  auto it = map.find(key);
  if (it == map.end()) {
    fail_with(
        fpos, [&](auto &os) { os << "key not found: " << esc{key}; });
  }
  return it->second;
}

static auto &operator<<(std::ostream &os, const esc_type &esc) {
  auto &value = esc.value;
  auto name = find(POS, type_names, value);
  return os << name;
}

static auto &operator<<(std::ostream &os, const esc_code &e) {
  return os << hex{e.value};
}

static auto &operator<<(std::ostream &os, const esc<input_event> &e) {
  auto &ev = e.value;
  return os << "(input_event " << esc_type{ev.type} << ' '
            << esc_code{ev.code} << ' ' << ev.value << ')';
}

static auto open_input(const auto &pathname, const auto &report_ff) {
  auto flags = report_ff ? O_RDWR : O_RDONLY;
  return s_open(pathname, flags);
}

static auto get_bits(
    const auto &ev, const auto &type, const auto &type_max) {
  auto bits = std::vector<uint8_t>{};
  bits.resize(to_unsigned(type_max) / 8U + 1U);
  check(
      POS,
      ioctl(ev.get(), EVIOCGBIT(type, type_max), bits.data()),
      "EVIOCGBIT failed");
  return bits;
}

static auto has_bit(const auto &bits, const auto &bit) {
  auto ubit = to_unsigned(bit);
  return (bits[ubit / 8U] & 1 << ubit % 8U) != 0U;
}

static auto get_type_bits(const auto &ev) {
  return get_bits(ev, EV_SYN, EV_MAX);
}

static auto get_abs_bits(const auto &ev) {
  return get_bits(ev, EV_ABS, ABS_MAX);
}

static auto get_ff_bits(const auto &fd) {
  return get_bits(fd, EV_FF, FF_MAX);
}

static auto get_ff_memsize(const auto &fd) {
  auto n = int{};
  check(
      POS,
      ioctl(fd.get(), EVIOCGEFFECTS, &n) == 0,
      "EVIOCGEFFECTS failed");
  return n;
}

struct ff_capabilities_t {
  std::set<code_t> bits;
  uint memsize;
};

static auto get_ff_capabilities(const auto &fd) {
  auto capa = ff_capabilities_t{};
  auto ff_bits = get_ff_bits(fd);
  for (auto i = code_t{0U}; i < FF_MAX; i++) {
    if (has_bit(ff_bits, i)) {
      capa.bits.insert(i);
    }
  }
  capa.memsize = to_unsigned(get_ff_memsize(fd));
  return capa;
}

class vj_t {
 public:
  vj_t() : _fd{nullptr} {}
  vj_t(const auto &fd)
    requires(!std::is_same_v<RAWTYPE(fd), vj_t>)
      : _fd{&fd} {
    check(
        POS,
        ioctl(this->_fd->get(), UI_DEV_CREATE) >= 0,
        "UI_DEV_CREATE failed");
  }
  vj_t(vj_t &&other) { *this = std::move(other); }
  ~vj_t() {
    if (*this) {
      ioctl(this->_fd->get(), UI_DEV_DESTROY);
    }
  }
  vj_t &operator=(vj_t &&other) {
    this->_fd = std::exchange(other._fd, nullptr);
    return *this;
  }
  const auto &get() const { return this->_fd; }
  explicit operator bool() const { return this->_fd; }
  vj_t(const vj_t &) = delete;
  vj_t &operator=(const vj_t &) = delete;

 private:
  const fd_t *_fd;
};

static constexpr auto default_abs_output =
    abs_output_t{.min = -32767, .max = 32767, .zero = 0, .unit = 32767};

static auto memcpy(auto &out, const auto &in) {
  static_assert(std::is_same_v<RAWTYPE(out), RAWTYPE(in)>);
  memcpy(&out, &in, sizeof(out));
}

template<class Tabs_value>
struct controller_state_t {
  std::map<code_t, value_t> keys;
  std::map<code_t, Tabs_value> abss;
  bool dirty = false;
};

static auto get_map(
    const auto &def, const auto &map, const auto &code) {
  auto value = find_opt(map, code);
  return value ? *value : def;
}

static auto set_map(auto &map, const auto &code, const auto &value) {
  if (!map.contains(code) || map[code] != value) {
    map[code] = value;
    return true;
  } else {
    return false;
  }
}

static auto report_error(const auto &err) {
  std::cerr << err.pos.file << ':' << err.pos.line << ": "
            << err.message << std::endl;
}

static auto log_event_type(const auto &type) {
  return type != EV_SYN && type != EV_ABS && type != EV_KEY;
}

static auto recv_event(const auto &fd, auto &ev) {
  full_read(fd, ev);
  fdebug_with(2, [&](const auto &fline) {
    if (log_event_type(ev.type)) {
      fline([&](auto &dbg) { dbg << "recv " << fd << ' ' << esc{ev}; });
    }
  });
  return ev;
}

static auto send_event(const auto &fd, const auto &ev) {
  fdebug_with(2, [&](auto &fline) {
    if (log_event_type(ev.type)) {
      fline([&](auto &dbg) { dbg << "send " << fd << ' ' << esc{ev}; });
    }
  });
  full_write(fd, ev);
}

class lvj_t {
 public:
  lvj_t(const auto &output, const auto &ff_capabilities) :
      _output{output},
      _has_ff{ff_capabilities.any()},
      _fd{s_open(
          "/dev/uinput", ff_capabilities.any() ? O_RDWR : O_WRONLY)} {
    auto &keys = output.keys;
    auto &abss = output.abss;
    if (!keys.empty()) {
      check(
          POS,
          ioctl(this->_fd.get(), UI_SET_EVBIT, EV_KEY) == 0,
          "UI_SET_EVBIT failed");
      for (auto &code : keys) {
        check(
            POS,
            ioctl(this->_fd.get(), UI_SET_KEYBIT, code) == 0,
            "UI_SET_KEYBIT failed");
      }
    }
    if (!abss.empty()) {
      check(
          POS,
          ioctl(this->_fd.get(), UI_SET_EVBIT, EV_ABS) == 0,
          "UI_SET_EVBIT failed");
      for (auto &[code, abs] : abss) {
        check(
            POS,
            ioctl(this->_fd.get(), UI_SET_ABSBIT, code) == 0,
            "UI_SET_ABSBIT failed");
        auto &abs_output = abs.fold(
            []() -> auto & { return default_abs_output; },
            [](const auto &abs_output) -> auto & {
              return abs_output;
            });
        auto abs_setup = uinput_abs_setup{};
        zero(abs_setup);
        abs_setup.code = code;
        abs_setup.absinfo.minimum = abs_output.min;
        abs_setup.absinfo.maximum = abs_output.max;
        check(
            POS,
            ioctl(this->_fd.get(), UI_ABS_SETUP, &abs_setup) == 0,
            "UI_ABS_SETUP failed");
      }
    }
    ff_capabilities.iter([&](const auto &ff_capabilities) {
      check(
          POS,
          ioctl(this->_fd.get(), UI_SET_EVBIT, EV_FF) == 0,
          "UI_SET_EVBIT failed");
      for (auto &code : ff_capabilities.bits) {
        check(
            POS,
            ioctl(this->_fd.get(), UI_SET_FFBIT, code) == 0,
            "UI_SET_FFBIT failed");
      }
    });
    auto setup = uinput_setup{};
    zero(setup);
    memcpy(setup.id, output.id);
    strncpy(setup.name, output.name.data(), sizeof(setup.name) - 1U);
    setup.ff_effects_max = ff_capabilities.fold(
        []() { return 0U; },
        [](const auto &ff_capabilities) {
          return ff_capabilities.memsize;
        });
    check(
        POS,
        ioctl(this->_fd.get(), UI_DEV_SETUP, &setup) >= 0,
        "UI_DEV_SETUP failed");
    this->_vj = vj_t{this->_fd};
  }
  const auto &has_ff() const { return this->_has_ff; }
  const auto &fd() const { return this->_fd; }
  auto set(const auto &virtual_event) {
    auto &type = virtual_event.common.type;
    if (type == EV_KEY) {
      auto &key_virtual_event = virtual_event.key;
      auto &code = key_virtual_event.code;
      if (this->_output.keys.contains(code)) {
        auto &value = key_virtual_event.value;
        if (set_map(this->_state.keys, code, value)) {
          this->emit_key(code, value);
          this->_state.dirty = true;
        }
      }
    } else if (type == EV_ABS) {
      auto &abs_virtual_event = virtual_event.abs;
      auto &code = abs_virtual_event.code;
      if (this->_output.abss.contains(code)) {
        auto &value = abs_virtual_event.value;
        auto ivalue = apply_abs_output(code, value, this->_output);
        ivalue.iter([&](const auto &ivalue) {
          if (set_map(this->_state.abss, code, ivalue)) {
            this->emit_abs(code, ivalue);
            this->_state.dirty = true;
          }
        });
      }
    } else if (type == EV_SYN) {
      if (this->_state.dirty) {
        this->_state.dirty = false;
        this->emit_syn();
      }
    } else {
      never(POS);
    }
  }

 private:
  const output_t &_output;
  bool _has_ff;
  fd_t _fd;
  vj_t _vj;
  controller_state_t<value_t> _state;
  auto emit_event(const auto &ev) const {
    try {
      send_event(this->_fd.get(), ev);
    } catch (const err_t &err) {
      report_error(err);
      fail_with(POS, [&](auto &os) {
        os << "failed to send event fd:" << this->_fd.get()
           << " type:" << esc_type{ev.type} << " code:" << ev.code
           << " value:" << ev.value;
      });
    }
  }
  void emit_key(const auto &code, const auto value) const {
    auto ev = input_event{.type = EV_KEY, .code = code, .value = value};
    this->emit_event(ev);
  }
  void emit_abs(const auto &code, const auto value) const {
    auto ev = input_event{.type = EV_ABS, .code = code, .value = value};
    this->emit_event(ev);
  }
  void emit_syn() const {
    auto ev =
        input_event{.type = EV_SYN, .code = SYN_REPORT, .value = 0};
    this->emit_event(ev);
  }
};

struct controller_id_t {
  enum type_t {
    INPUT,
    OUTPUT
  } type;
  size_t index;
};

union virtual_event_t {
  struct common_virtual_event_t {
    controller_id_t controller_id;
    type_t type;
  } common;
  struct syn_virtual_event_t {
    controller_id_t controller_id;
    type_t type;
  } syn;
  struct key_virtual_event_t {
    controller_id_t controller_id;
    type_t type;
    code_t code;
    value_t value;
  } key;
  struct abs_virtual_event_t {
    controller_id_t controller_id;
    type_t type;
    code_t code;
    flt_t value;
  } abs;
  struct ff_virtual_event_t {
    controller_id_t controller_id;
    type_t type;
    code_t code;
    value_t value;
  } ff;
  struct uinput_virtual_event_t {
    controller_id_t controller_id;
    type_t type;
    code_t code;
    value_t value;
  } uinput;
};

struct source_id_t {
  size_t controller_index;
  type_t type;
  code_t code;
};

static auto operator==(const source_id_t &s1, const source_id_t &s2) {
  return s1.controller_index == s2.controller_index
         && s1.type == s2.type && s1.code == s2.code;
}

template<>
struct std::hash<source_id_t> {
  auto operator()(const auto &source_id) const {
    return auto_hasher_f(
        source_id.controller_index, source_id.type, source_id.code);
  }
};

class virtual_controller_t {
 public:
  auto set(const auto &virtual_event) {
    auto &type = virtual_event.common.type;
    if (type == EV_KEY) {
      auto &key_virtual_event = virtual_event.key;
      auto &code = key_virtual_event.code;
      auto &value = key_virtual_event.value;
      if (set_map(this->_state.keys, code, value)) {
        this->_state.dirty = true;
        return true;
      } else {
        return false;
      }
    } else if (type == EV_ABS) {
      auto &abs_virtual_event = virtual_event.abs;
      auto &code = abs_virtual_event.code;
      auto &value = abs_virtual_event.value;
      if (set_map(this->_state.abss, code, value)) {
        this->_state.dirty = true;
        return true;
      } else {
        return false;
      }
    } else if (type == EV_SYN) {
      if (this->_state.dirty) {
        this->_state.dirty = false;
        return true;
      } else {
        return false;
      }
    } else {
      return never<bool>(POS);
    }
  }
  auto set_abs_source(
      const auto &source_id, const auto &code, const auto &value) {
    // todo: maybe precompute source list
    auto &sources = this->_sources[code];
    sources[source_id] = value;
    auto s = fold(
        flt_t{0}, sources, [](const auto &accu, const auto &value) {
          return accu + value.second;
        });
    return std::clamp(s, AS(s, -1), AS(s, 1));
  }

 private:
  controller_state_t<flt_t> _state;
  std::unordered_map<code_t, std::unordered_map<source_id_t, flt_t>>
      _sources;
};

using effect_id_t =
    std::make_unsigned_t<RAWTYPE(std::declval<ff_effect>().id)>;

using effect_type_t = RAWTYPE(std::declval<ff_effect>().type);

struct ff_mapping_data_t {
  std::unordered_map<effect_id_t, effect_id_t> input_id_of_output_id;
};

struct state_t {
  config_t config;
  std::vector<fd_t> inputs;
  std::vector<lvj_t> outputs;
  std::vector<virtual_controller_t> controllers;
  std::unordered_map<const abs_to_keys_t *, code_t> values;
  std::unordered_map<const ff_mapping_t *, ff_mapping_data_t>
      ff_mapping_datas;
  std::vector<pollfd> poll_fds;
  std::vector<controller_id_t> poll_fds_ids;
};

static auto init_poll_fds(auto &state) {
  auto &inputs = state.inputs;
  auto &outputs = state.outputs;
  auto &poll_fds = state.poll_fds;
  auto &poll_fds_ids = state.poll_fds_ids;
  iteri(inputs, [&](const auto &i, const auto &input) {
    poll_fds.push_back(pollfd{.fd = input.get(), .events = POLLIN});
    poll_fds_ids.push_back(
        controller_id_t{.type = controller_id_t::INPUT, .index = i});
  });
  iteri(outputs, [&](const auto &i, const auto &output) {
    if (output.has_ff()) {
      // todo: .fd().get() really?
      poll_fds.push_back(
          pollfd{.fd = output.fd().get(), .events = POLLIN});
      poll_fds_ids.push_back(
          controller_id_t{.type = controller_id_t::OUTPUT, .index = i});
    }
  });
}

static auto poll_events(auto &ev, auto &state) {
  auto &poll_fds = state.poll_fds;
  s_poll(poll_fds.data(), poll_fds.size(), -1);
  auto evs = std::vector<std::pair<controller_id_t, input_event>>{};
  iteri(poll_fds, [&](const auto &i, const auto &poll_fd) {
    if ((poll_fd.revents & POLLIN) == POLLIN) {
      auto &controller_id = state.poll_fds_ids[i];
      recv_event(poll_fd.fd, ev);
      evs.push_back(std::pair{controller_id, ev});
    }
  });
  assert(POS, !evs.empty());
  return evs;
}

static auto lookup(const auto &table, const auto &vx) {
  static_assert(std::is_same_v<RAWTYPE(vx), RAWTYPE(table.x[0U])>);
  auto &x = table.x;
  auto &y = table.y;
  auto sz = x.size();
  assert(POS, y.size() == sz);
  auto l = AS(sz, 0U);
  auto r = AS(sz, sz - 1U);
loop:
  if (l == r) return y[l];
  auto m = (l + r) / 2U;
  if (x[m] == vx) return y[m];
  if (x[m] < vx) {
    if (m == l) {
      goto interp;
    }
    l = m;
  }
  if (x[m] > vx) {
    r = m;
  }
  goto loop;
interp:
  check(POS, r == l + 1U, "DICHO");
  auto x0 = x[l];
  auto x1 = x[r];
  if (vx >= x1) {
    return y[r];
  }
  auto k = flt_t{vx - x0} / flt_t{x1 - x0};
  return (1 - k) * y[l] + k * y[r];
}

static auto apply_key_mapping(
    auto &state,
    const auto &key_virtual_event,
    const auto &target_controller,
    const auto &mapping) {
  auto &source_controller_id = key_virtual_event.controller_id;
  assert(POS, source_controller_id.type == controller_id_t::INPUT);
  assert(POS, target_controller.type == controller_id_t::INPUT);
  auto &target_controller_index = target_controller.index;
  auto &source_controller_index = source_controller_id.index;
  auto res = std::vector<virtual_event_t>{};
  auto &code = key_virtual_event.code;
  auto &value = key_virtual_event.value;
  auto new_code = find_opt(mapping.keys, code);
  static_assert(std::is_same_v<
                decltype(new_code),
                option_t<const option_t<code_t, false>, true>>);
  auto new_virtual_event = virtual_event_t{};
  new_virtual_event.common.controller_id = controller_id_t{
      .type = controller_id_t::INPUT, .index = target_controller_index};
  // todo: factorize the key to key mapping code
  new_code.iter(
      [&]() {
        if (mapping.keep_other_keys) {
          auto &key_virtual_event = new_virtual_event.key;
          key_virtual_event.type = EV_KEY;
          key_virtual_event.code = code;
          key_virtual_event.value = value;
          res.push_back(new_virtual_event);
        }
      },
      [&](const auto &new_code) {
        static_assert(std::is_same_v<
                      decltype(new_code),
                      const option_t<code_t, false> &>);
        auto &key_virtual_event = new_virtual_event.key;
        key_virtual_event.type = EV_KEY;
        key_virtual_event.code = new_code.fold(
            [&]() { return code; },
            [](const auto &new_code) {
              static_assert(
                  std::is_same_v<decltype(new_code), const code_t &>);
              return new_code;
            });
        key_virtual_event.value = value;
        res.push_back(new_virtual_event);
      });
  auto key_to_abs = find_opt(mapping.keys_to_abss, code);
  key_to_abs.iter([&](const auto &key_to_abs) {
    auto new_value = value * key_to_abs.value;
    auto &target_controller =
        state.controllers[target_controller_index];
    auto source_id = source_id_t{
        .controller_index = source_controller_index,
        .type = EV_KEY,
        .code = code};
    auto merged_value = target_controller.set_abs_source(
        source_id, key_to_abs.code, new_value);
    auto &abs_virtual_event = new_virtual_event.abs;
    abs_virtual_event.type = EV_ABS;
    abs_virtual_event.code = key_to_abs.code;
    abs_virtual_event.value = merged_value;
    res.push_back(new_virtual_event);
  });
  return res;
}

static auto apply_abs_to_keys(
    auto &res,
    const auto &value,
    const auto &dst_controller_id,
    const auto &abs_to_keys,
    auto &values) {
  auto last_key = values.contains(&abs_to_keys)
                      ? option_t{values[&abs_to_keys]}
                      : option_t<code_t>{};
  auto new_zone = (const abs_to_key_zone_t *)nullptr;
  for (auto &zone : abs_to_keys) {
    if (zone.code == last_key) {
      if (zone.left <= value && value <= zone.right) {
        return;
      }
    } else {
      if (zone.left <= value && value <= zone.right) {
        new_zone = &zone;
      }
    }
  }
  check(POS, new_zone, "no new zone for abs to keys");
  new_zone->code.iter(
      [&]() { values.erase(&abs_to_keys); },
      [&](const auto &code) { values[&abs_to_keys] = code; });
  auto dst_virtual_event = virtual_event_t{};
  auto &dst_abs_virtual_event = dst_virtual_event.key;
  dst_abs_virtual_event.controller_id = dst_controller_id;
  dst_abs_virtual_event.type = EV_KEY;
  last_key.iter([&](const auto &last_key) {
    dst_abs_virtual_event.code = last_key;
    dst_abs_virtual_event.value = 0;
    res.push_back(dst_virtual_event);
  });
  new_zone->code.iter(
      []() {},
      [&](const auto &code) {
        dst_abs_virtual_event.code = code;
        dst_abs_virtual_event.value = 1;
        res.push_back(dst_virtual_event);
      });
}

// todo: factorize
static auto apply_abs_mapping(
    auto &state,
    const auto &abs_virtual_event,
    const auto &dst_controller_id,
    const auto &mapping,
    auto &values) {
  auto &src_controller_id = abs_virtual_event.controller_id;
  assert(POS, src_controller_id.type == controller_id_t::INPUT);
  auto &src_controller_index = src_controller_id.index;
  auto res = std::vector<virtual_event_t>{};
  auto &src_code = abs_virtual_event.code;
  auto &src_value = abs_virtual_event.value;
  assert(POS, dst_controller_id.type == controller_id_t::INPUT);
  auto &dst_controller_index = dst_controller_id.index;
  auto &dst_controller = state.controllers[dst_controller_index];
  auto source_id = source_id_t{
      .controller_index = src_controller_index,
      .type = EV_ABS,
      .code = src_code};
  if (auto _abs_mapping = mapping.abss.find(src_code);
      _abs_mapping != mapping.abss.end()) {
    auto &abs_mapping = _abs_mapping->second;
    auto dst_code = abs_mapping.code.fold(
        [&]() { return src_code; },
        [](const auto &code) { return code; });
    auto dst_value = abs_mapping.table.fold(
        [&]() { return abs_virtual_event.value; },
        [&](const auto &table) { return lookup(table, src_value); });
    auto merged_value =
        dst_controller.set_abs_source(source_id, dst_code, dst_value);
    auto dst_virtual_event = virtual_event_t{};
    auto &dst_abs_virtual_event = dst_virtual_event.abs;
    dst_abs_virtual_event.controller_id = dst_controller_id;
    dst_abs_virtual_event.type = EV_ABS;
    dst_abs_virtual_event.code = dst_code;
    dst_abs_virtual_event.value = merged_value;
    res.push_back(dst_virtual_event);
  } else if (mapping.keep_other_abss) {
    auto merged_value =
        dst_controller.set_abs_source(source_id, src_code, src_value);
    auto dst_virtual_event = virtual_event_t{};
    auto &dst_abs_virtual_event = dst_virtual_event.abs;
    dst_abs_virtual_event.controller_id = dst_controller_id;
    dst_abs_virtual_event.type = EV_ABS;
    dst_abs_virtual_event.code = src_code;
    dst_abs_virtual_event.value = merged_value;
    res.push_back(dst_virtual_event);
  }
  auto abs_to_keys = find_opt(mapping.abss_to_keys, src_code);
  abs_to_keys.iter([&](const auto &abs_to_keys) {
    apply_abs_to_keys(
        res, src_value, dst_controller_id, abs_to_keys, values);
  });
  return res;
}

static auto apply_abs_output(
    const auto &code, const auto &value, const auto &output) {
  auto abs_output_opt = find_opt(output.abss, code);
  return abs_output_opt.fold(
      []() { return option_t<value_t>{}; },
      [&](const auto &abs_output_opt) {
        auto &abs_output = abs_output_opt.fold(
            []() -> auto & { return default_abs_output; },
            [](const auto &abs_output_opt) -> auto & {
              return abs_output_opt;
            });
        auto new_value = abs_output.unit * value + abs_output.zero;
        new_value = std::clamp(
            new_value, flt_t{abs_output.min}, flt_t{abs_output.max});
        new_value = std::round(new_value);
        return option_t<value_t>{(value_t)new_value};
      });
}

template<class T>
class queue_t {
 public:
  auto push(auto &&item) { this->_queue.push(FWD(item)); }
  auto pop() {
    auto item = std::move(this->_queue.front());
    this->_queue.pop();
    return item;
  }
  auto empty() const { return this->_queue.empty(); }

 private:
  std::queue<T> _queue;
};

static std::ostream &operator<<(
    std::ostream &os, const esc<controller_id_t> &e) {
  auto &controller_id = e.value;
  auto stype = pointer_t<const char>{};
  switch (controller_id.type) {
    case controller_id_t::INPUT:
      stype = "INPUT";
      break;
    case controller_id_t::OUTPUT:
      stype = "OUTPUT";
      break;
    default:
      never(POS);
  }
  return os << "(controller_id " << stype << ' ' << controller_id.index
            << ')';
}

static std::ostream &operator<<(
    std::ostream &os,
    const esc<virtual_event_t::syn_virtual_event_t> &e) {
  auto &ev = e.value;
  return os << "(sync_virtual_event " << esc{ev.controller_id} << ' '
            << esc_type{ev.type} << ')';
}

static std::ostream &operator<<(
    std::ostream &os,
    const esc<virtual_event_t::key_virtual_event_t> &e) {
  auto &ev = e.value;
  return os << "(key_virtual_event " << esc{ev.controller_id} << ' '
            << esc_type{ev.type} << ' ' << esc_code{ev.code} << ' '
            << ev.value << ')';
}

static std::ostream &operator<<(
    std::ostream &os,
    const esc<virtual_event_t::abs_virtual_event_t> &e) {
  auto &ev = e.value;
  return os << "(abs_virtual_event " << esc{ev.controller_id} << ' '
            << esc_type{ev.type} << ' ' << esc_code{ev.code} << ' '
            << ev.value << ')';
}

static std::ostream &operator<<(
    std::ostream &os,
    const esc<virtual_event_t::ff_virtual_event_t> &e) {
  auto &ev = e.value;
  return os << "(ff_virtual_event " << esc{ev.controller_id} << ' '
            << esc_type{ev.type} << ' ' << esc_code{ev.code} << ' '
            << ev.value << ')';
}

static std::ostream &operator<<(
    std::ostream &os,
    const esc<virtual_event_t::uinput_virtual_event_t> &e) {
  auto &ev = e.value;
  return os << "(uinput_virtual_event " << esc{ev.controller_id} << ' '
            << esc_type{ev.type} << ' ' << esc_code{ev.code} << ' '
            << ev.value << ')';
}

static auto &operator<<(
    std::ostream &os, const esc<virtual_event_t> &e) {
  auto &ev = e.value;
  switch (ev.common.type) {
    case EV_SYN:
      os << esc{ev.syn};
      break;
    case EV_KEY:
      os << esc{ev.key};
      break;
    case EV_ABS:
      os << esc{ev.abs};
      break;
    case EV_FF:
      os << esc{ev.ff};
      break;
    case EV_UINPUT:
      os << esc{ev.uinput};
      break;
    default:
      never(POS);
  }
  return os;
}

static auto to_syn_virtual_event(
    auto &syn_event, const auto &controller_id, const auto &ev) {
  if (ev.code == SYN_REPORT) {
    assert(POS, controller_id.type == controller_id_t::INPUT);
    syn_event.controller_id = controller_id;
    syn_event.type = EV_SYN;
    return true;
  } else {
    return false;
  }
}

static auto to_key_virtual_event(
    const auto &state,
    auto &key_event,
    const auto &controller_id,
    const auto &ev) {
  // only events with value 0 or 1 have meanings
  if (ev.value != 0 && ev.value != 1) {
    return false;
  }
  assert(POS, controller_id.type == controller_id_t::INPUT);
  auto &input_config = state.config.inputs[controller_id.index];
  auto keep_it = input_config.keys.fold(
      []() { return true; },
      [&](const auto &keys) { return keys.contains(ev.code); });
  if (keep_it) {
    key_event.controller_id = controller_id;
    key_event.type = EV_KEY;
    key_event.code = ev.code;
    key_event.value = ev.value;
    return true;
  } else {
    return false;
  }
}

static auto to_abs_virtual_event(
    const auto &state,
    auto &abs_event,
    const auto &controller_id,
    const auto &ev) {
  assert(POS, controller_id.type == controller_id_t::INPUT);
  assert(POS, controller_id.index < state.config.inputs.size());
  auto &input_config = state.config.inputs[controller_id.index];
  if (!input_config.abss.contains(ev.code)) {
    return false;
  }
  auto &table = find(POS, input_config.abss, ev.code);
  abs_event.controller_id = controller_id;
  abs_event.type = EV_ABS;
  abs_event.code = ev.code;
  abs_event.value = lookup(table, ev.value);
  return true;
}

static auto to_ff_virtual_event(
    auto &ff_event, const auto &controller_id, const auto &ev) {
  if (controller_id.type != controller_id_t::OUTPUT) {
    // todo: is it correct to ignore such thing?
    return false;
  }
  assert(POS, controller_id.type == controller_id_t::OUTPUT);
  ff_event.controller_id = controller_id;
  ff_event.type = EV_FF;
  ff_event.code = ev.code;
  ff_event.value = ev.value;
  return true;
}

static auto to_uinput_virtual_event(
    auto &uinput_event, const auto &controller_id, const auto &ev) {
  assert(POS, controller_id.type == controller_id_t::OUTPUT);
  uinput_event.controller_id = controller_id;
  uinput_event.type = EV_UINPUT;
  uinput_event.code = ev.code;
  uinput_event.value = ev.value;
  return true;
}

static auto to_virtual_event(
    const auto &state, auto &virtual_event, const auto &ev) {
  fdebug_with(3, [&](auto &fline) {
    if (log_event_type(ev.second.type)) {
      fline([&](auto &dbg) { dbg << "to_virtual_event " << esc{ev}; });
    }
  });
  switch (ev.second.type) {
    case EV_SYN:
      return to_syn_virtual_event(
          virtual_event.syn, ev.first, ev.second);
    case EV_KEY:
      return to_key_virtual_event(
          state, virtual_event.key, ev.first, ev.second);
    case EV_ABS:
      return to_abs_virtual_event(
          state, virtual_event.abs, ev.first, ev.second);
    case EV_FF:
      return to_ff_virtual_event(virtual_event.ff, ev.first, ev.second);
    case EV_UINPUT:
      return to_uinput_virtual_event(
          virtual_event.uinput, ev.first, ev.second);
    default:
      return false;
  }
}

struct esc_effect_type {
  const effect_type_t &value;
};

static const auto ff_effect_type_names =
    std::unordered_map<effect_type_t, const char *>{
        {0U, "FF_NO_TYPE"},
        {FF_RUMBLE, "FF_RUMBLE"},
        {FF_PERIODIC, "FF_PERIODIC"},
        {FF_CONSTANT, "FF_CONSTANT"},
        {FF_SPRING, "FF_SPRING"},
        {FF_FRICTION, "FF_FRICTION"},
        {FF_DAMPER, "FF_DAMPER"},
        {FF_INERTIA, "FF_INERTIA"},
        {FF_RAMP, "FF_RAMP"}};

static auto &operator<<(std::ostream &os, const esc_effect_type &e) {
  auto name = find(POS, ff_effect_type_names, e.value);
  return os << name;
}

static auto &operator<<(std::ostream &os, const esc<ff_trigger> &e) {
  auto &t = e.value;
  return os << "(trigger " << hex{t.button} << ' ' << t.interval << ')';
}

static auto &operator<<(std::ostream &os, const esc<ff_replay> &e) {
  auto &r = e.value;
  return os << "(replay " << r.length << ' ' << r.delay << ')';
}

static auto &operator<<(
    std::ostream &os, const esc<ff_rumble_effect> &e) {
  auto &r = e.value;
  return os << "(rumble_effect " << r.strong_magnitude << ' '
            << r.weak_magnitude << ')';
}

static auto &operator<<(std::ostream &os, const esc<ff_effect> &e) {
  auto &ffe = e.value;
  os << "(effect " << esc_effect_type{ffe.type} << " (id " << ffe.id
     << ") (direction " << hex{ffe.direction} << ") "
     << esc{ffe.trigger} << ' ' << esc{ffe.replay} << ' ';
  switch (ffe.type) {
    case 0U:
      break;
    case FF_RUMBLE:
      os << esc{ffe.u.rumble};
      break;
    default:
      fail_with(POS, [&](auto &os) {
        os << "unknown effect type " << esc_effect_type{ffe.type};
      });
  }
  return os << ')';
}

static auto process_output_event(
    auto &state, const auto &virtual_event) {
  auto &common_event = virtual_event.common;
  auto oid = common_event.controller_id.index;
  auto ofd = state.outputs[oid].fd().get();
  auto ff_mappings =
      bind(state.config.ff_mappings, [&](const auto &ff_mapping) {
        if (ff_mapping.output_controller == oid) {
          return option_t{&ff_mapping};
        } else {
          return option_t<const ff_mapping_t *>{};
        }
      });
  assert(POS, ff_mappings.size() == 1U);
  auto &ff_mapping = *ff_mappings[0U];
  auto ifd = state.inputs[ff_mapping.input_controller].get();
  auto &ff_mapping_data = state.ff_mapping_datas[&ff_mapping];
  if (virtual_event.common.type == EV_UINPUT) {
    auto &uinput_event = virtual_event.uinput;
    if (uinput_event.code == UI_FF_UPLOAD) {
      // todo: factorize code
      auto ff = uinput_ff_upload{};
      zero(ff);
      ff.request_id = as_unsigned(POS, uinput_event.value);
      check(
          POS,
          ioctl(ofd, UI_BEGIN_FF_UPLOAD, &ff) == 0,
          "UI_BEGIN_FF_UPLOAD failed");
      fdebug(2, [&](auto &dbg) { dbg << "UI_BEGIN_FF_UPLOAD sent"; });
      check(POS, ff.retval == 0, "retval non zero");
      fdebug_with(2, [&](const auto &fline) {
        fline([&](auto &dbg) {
          dbg << "uploading new " << esc{ff.effect};
        });
        fline(
            [&](auto &dbg) { dbg << "uploading old " << esc{ff.old}; });
      });
      auto is_new = ff.old.type == 0;
      auto output_id = as_unsigned(POS, ff.effect.id);
      // todo: new means unregistered in input_id_of_output_id?
      if (is_new) {
        ff.effect.id = -1;
      } else {
        ff.effect.id = as_signed(
            POS, ff_mapping_data.input_id_of_output_id.at(output_id));
      }
      if (ff_mapping.clamp_duration
          && ff.effect.replay.length > 0x7fff) {
        fdebug(2, [&](auto &dbg) {
          dbg << "clamping effect length " << ff.effect.replay.length;
        });
        ff.effect.replay.length = 0x7fff;
      }
      check(
          POS,
          ioctl(ifd, EVIOCSFF, &ff.effect) == 0,
          "EVIOCSFF failed");
      fdebug(2, [&](auto &dbg) { dbg << "EVIOCSFF sent"; });
      if (is_new) {
        fdebug(2, [&](auto &dbg) {
          dbg << "effect id mapping input:" << ff.effect.id
              << " output:" << output_id;
        });
        ff_mapping_data.input_id_of_output_id[output_id] =
            as_unsigned(POS, ff.effect.id);
      }
      ff.effect.id = as_signed(POS, output_id);
      check(
          POS,
          ioctl(ofd, UI_END_FF_UPLOAD, &ff) == 0,
          "UI_END_FF_UPLOAD failed");
      fdebug(2, [&](auto &dbg) { dbg << "UI_END_FF_UPLOAD sent"; });
    } else if (uinput_event.code == UI_FF_ERASE) {
      auto ff = uinput_ff_erase{};
      zero(ff);
      ff.request_id = as_unsigned(POS, uinput_event.value);
      check(
          POS,
          ioctl(ofd, UI_BEGIN_FF_ERASE, &ff) == 0,
          "UI_BEGIN_FF_ERASE failed");
      fdebug(2, [&](auto &dbg) { dbg << "UI_BEGIN_FF_ERASE sent"; });
      check(POS, ff.retval == 0, "retval non zero");
      auto output_effect_id = safe_cast<effect_id_t>(POS, ff.effect_id);
      auto input_effect_id = find(
          POS, ff_mapping_data.input_id_of_output_id, output_effect_id);
      fdebug(2, [&](auto &dbg) {
        dbg << "remove effect id mapping input:" << input_effect_id
            << " output:" << output_effect_id;
      });
      ff_mapping_data.input_id_of_output_id.erase(output_effect_id);
      check(
          POS,
          ioctl(ifd, EVIOCRMFF, input_effect_id) == 0,
          "EVIOCRMFF failed");
      fdebug(2, [&](auto &dbg) { dbg << "EVIOCRMFF sent"; });
      check(
          POS,
          ioctl(ofd, UI_END_FF_ERASE, &ff) == 0,
          "UI_END_FF_ERASE failed");
      fdebug(2, [&](auto &dbg) { dbg << "UI_END_FF_ERASE sent"; });
    } else {
      fail_with(POS, [&](auto &os) {
        os << "unknown UINPUT_EVENT code "
           << esc_code{uinput_event.code};
      });
    }
  } else if (virtual_event.common.type == EV_FF) {
    auto &ff_event = virtual_event.ff;
    auto &code = ff_event.code;
    auto &value = ff_event.value;
    auto i_effect_id =
        find_opt(ff_mapping_data.input_id_of_output_id, code);
    auto rev = input_event{};
    zero(rev);
    rev.type = EV_FF;
    rev.code = i_effect_id.fold(
        [&]() { return code; },
        [](const auto &i_effect_id) { return i_effect_id; });
    rev.value = value;
    send_event(ifd, rev);
  } else {
    fail_with(POS, [&](auto &os) {
      os << "unknown output event type "
         << esc_type{virtual_event.common.type};
    });
  }
}

static auto process_input_event(auto &state, const auto &ev) {
  auto virtual_event = virtual_event_t{};
  if (!to_virtual_event(state, virtual_event, ev)) {
    return;
  }
  auto todo = queue_t<virtual_event_t>{};
  // todo: assuming virtual_event.source.controller and the target
  // controller are the same. Yes it's true today.
  todo.push(virtual_event);
  auto &config = state.config;
  auto &mappings = config.mappings;
  auto &output_mappings = config.output_mappings;
  while (!todo.empty()) {
    auto virtual_event = todo.pop();
    fdebug_with(3, [&](auto &fline) {
      if (log_event_type(virtual_event.common.type)) {
        fline([&](auto &dbg) {
          dbg << "virtual_event " << esc{virtual_event};
        });
      }
    });
    auto &src = virtual_event.common.controller_id;
    if (src.type == controller_id_t::OUTPUT) {
      process_output_event(state, virtual_event);
    } else if (state.controllers[src.index].set(virtual_event)) {
      if (auto virtual_mappings = mappings.find(src.index);
          virtual_mappings != mappings.end()) {
        for (auto &virtual_mapping : virtual_mappings->second) {
          auto &dst_index = virtual_mapping.controller;
          auto dst = controller_id_t{
              .type = controller_id_t::INPUT, .index = dst_index};
          auto &mapping = virtual_mapping.mapping;
          if (virtual_event.common.type == EV_KEY) {
            auto new_virtual_events = apply_key_mapping(
                state, virtual_event.key, dst, mapping);
            for (auto &new_virtual_event : new_virtual_events) {
              todo.push(new_virtual_event);
            }
          } else if (virtual_event.common.type == EV_ABS) {
            auto new_virtual_events = apply_abs_mapping(
                state, virtual_event.abs, dst, mapping, state.values);
            for (auto &new_virtual_event : new_virtual_events) {
              todo.push(new_virtual_event);
            }
          } else if (virtual_event.common.type == EV_SYN) {
            auto new_virtual_event = virtual_event_t{};
            auto &new_syn_virtual_event = new_virtual_event.syn;
            new_syn_virtual_event.controller_id = dst;
            new_syn_virtual_event.type = EV_SYN;
            todo.push(new_virtual_event);
          } else {
            never(POS);
          }
        }
      }
      if (auto output_idx = output_mappings.find(src.index);
          output_idx != output_mappings.end()) {
        auto &output = state.outputs[output_idx->second];
        output.set(virtual_event);
      }
    }
  }
}

class abs_t {
 public:
  auto data() const { return &this->_values[0U]; }
  const auto &min() const { return this->_values[1U]; }
  const auto &max() const { return this->_values[2U]; }

 private:
  value_t _values[5U];
};

static auto get_abs(const auto &ev, const auto &code) {
  auto abs = abs_t{};
  check(
      POS,
      ioctl(ev.get(), EVIOCGABS(code), abs.data()) == 0,
      "EVIOCGABS failed");
  return abs;
}

static auto fill_missing_input_tables(const auto &ev, auto &input) {
  if (input.keep_other_abss) {
    auto type_bits = get_type_bits(ev);
    if (has_bit(type_bits, EV_ABS)) {
      auto abs_bits = get_abs_bits(ev);
      for (auto code = code_t{0U}; code < ABS_MAX; code++) {
        if (has_bit(abs_bits, code) && !input.abss.contains(code)) {
          auto abs = get_abs(ev, code);
          auto sum = abs.min() + abs.max();
          if (sum % 2 != 0) {
            sum++;
          }
          auto middle = sum / 2;
          auto x = std::vector{abs.min(), abs.max()};
          auto y = std::vector<flt_t>{-1, 1};
          auto table = mk_dz(abs.min(), middle, middle, abs.max());
          input.abss[code] = table;
        }
      }
    }
  }
}

static auto fill_config(auto &state) {
  for (auto i = size_t{0U}; i < state.inputs.size(); i++) {
    auto &ev = state.inputs[i];
    auto &input = state.config.inputs[i];
    fill_missing_input_tables(ev, input);
  }
}

static const auto used_configs = {
    configs::flatout,
    configs::kbd_to_snes,
    configs::motion,
    configs::new_black,
    configs::new_black_sixaxis,
    configs::new_new_black,
    configs::pop,
    configs::raw_xbox,
    configs::xbox,
    configs::xbox_driving,
    mame::g29_vr,
    mame::new_black_vr,
    mame::new_new_black_vr,
    thrustmaster::hotas_4_config};

static auto get_configs() {
  auto configs = std::map<std::string, config_t>{};
  for (auto &config : used_configs) {
    assert(POS, !configs.contains(config.name));
    configs[config.name] = config;
  }
  auto res = std::vector<config_t>{};
  iteri(configs, [&](const auto &, const auto &config) {
    res.push_back(config);
  });
  return res;
}

static auto get_config(const auto &sel) {
  auto configs = get_configs();
  if (sel < configs.size()) {
    return configs[sel];
  } else {
    return fail<config_t>(POS, "unknown selection");
  }
}

static auto list_configs() {
  auto configs = get_configs();
  iteri(configs, [](const auto &i, const auto &config) {
    std::cout << i << '\t' << esc{config.name} << std::endl;
  });
}

static auto is_input_used_as_ff(const auto &config, const auto &i) {
  return any(config.ff_mappings, [&](const auto &ff_mapping) {
    return ff_mapping.input_controller == i;
  });
}

static auto get_output_ff_capabilities(
    const auto &ff_mappings, const auto &inputs, const auto &i) {
  for (auto &ff_mapping : ff_mappings) {
    if (ff_mapping.output_controller == i) {
      auto &input = inputs[ff_mapping.input_controller];
      auto ff_capabilities = get_ff_capabilities(input);
      return option_t{ff_capabilities};
    }
  }
  return option_t<ff_capabilities_t>{};
}

struct emulate_config_t {
  size_t config_index;
  std::vector<const char *> input_devices;
};

static auto emulate_joysticks(const auto &conf) {
  auto &cfg = conf.config_index;
  auto &input_devices = conf.input_devices;
  auto state = state_t{.config = get_config(cfg)};
  auto &config = state.config;
  fdebug(1, [&](auto &dbg) {
    dbg << "using config " << esc{config.name};
  });
  check(
      POS,
      input_devices.size() == config.inputs.size(),
      "invalid number of inputs");
  iteri(input_devices, [&](const auto &i, const auto &input) {
    auto report_ff = is_input_used_as_ff(config, i);
    state.inputs.push_back(open_input(input, report_ff));
  });
  auto &outputs = config.outputs;
  iteri(outputs, [&](const auto &i, const auto &output) {
    auto ff_capabilities =
        get_output_ff_capabilities(config.ff_mappings, state.inputs, i);
    state.outputs.push_back({output, ff_capabilities});
  });
  auto controller_count = config.inputs.size();
  for (auto &p : config.mappings) {
    for (auto &mapping : p.second) {
      controller_count =
          std::max(controller_count, mapping.controller + 1U);
    }
  }
  state.controllers.resize(controller_count);
  fill_config(state);
  init_poll_fds(state);
  auto ev = input_event{};
  while (true) {
    auto evs = poll_events(ev, state);
    for (auto &ev : evs) {
      process_input_event(state, ev);
    }
  }
}

static auto init_ostream(auto &os) {
  os << std::setfill('0')
     << std::setprecision(std::numeric_limits<flt_t>::max_digits10);
}

struct options_t {
  option_t<size_t> config_index;
  std::vector<const char *> inputs;
  maybe_t<_debug, option_t<int>> debug_level;
};

static auto parse_options(const auto &argc, const auto &argv) {
  auto opts = options_t{};
  for (auto i = AS(argc, 1); i < argc; i++) {
    auto &arg = argv[i];
    if (arg[0U] == '-') {
      auto report_unk = [&]() {
        fail_with(
            POS, [&](auto &os) { os << "unknown arg: " << esc{arg}; });
      };
      if constexpr (_debug) {
        if (strncmp(arg, "-d", 2U) == 0U) {
          auto dbg_lvl = parse<int>(POS, &arg[2U]);
          opts.debug_level = dbg_lvl;
        } else {
          report_unk();
        }
      } else {
        report_unk();
      }
    } else {
      if (opts.config_index.any()) {
        opts.inputs.push_back(arg);
      } else {
        auto conf_idx = parse<size_t>(POS, arg);
        opts.config_index = conf_idx;
      }
    }
  }
  return opts;
}

static auto process(const auto &argc, const auto &argv) {
  auto opts = parse_options(argc, argv);
  if constexpr (_debug) {
    opts.debug_level.iter(
        []() {}, [](const auto &dbg_lvl) { set_debug_level(dbg_lvl); });
  }
  init_ostream(std::cout);
  init_ostream(std::cerr);
  opts.config_index.iter(
      []() { list_configs(); },
      [&](const auto &config_index) {
        auto conf = emulate_config_t{
            .config_index = config_index, .input_devices = opts.inputs};
        emulate_joysticks(conf);
      });
}

int main(int argc, char *argv[]) {
  try {
    process(argc, argv);
    std::cout << std::flush;
    check(POS, !std::cout.fail(), "IO failure");
  } catch (const err_t &err) {
    report_error(err);
    return 1;
  } catch (...) {
    std::cerr << "unknown error" << std::endl;
    return 2;
  }
}
