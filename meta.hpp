#pragma once

#include <type_traits>

#define RAWTYPE(x) std::remove_cvref_t<decltype(x)>

#define FWD(x) (std::forward<decltype(x)>(x))

#define AS(ref, x) (RAWTYPE(ref){x})

#define CAST(ref, x) ((RAWTYPE(ref))(x))

template<class T>
concept Integer = std::is_integral_v<T>;

template<class T>
concept UnsignedInteger =
    std::is_unsigned_v<T> && std::is_integral_v<T>;

template<auto C, auto T, auto F>
struct iif;

template<auto C, auto T, auto F>
inline constexpr auto iif_v = iif<C, T, F>::value;

template<auto T, auto F>
struct iif<true, T, F> {
  static constexpr auto value = T;
};

template<auto T, auto F>
struct iif<false, T, F> {
  static constexpr auto value = F;
};

struct _void_t {};

template<auto C, class T>
using maybe_t = std::conditional_t<C, T, _void_t>;

template<class... Ts>
struct unify;

template<class... Ts>
using unify_t = unify<Ts...>::type;

template<class T>
struct unify<T> {
  using type = T;
};

template<class T>
struct unify<T, T> {
  using type = T;
};

template<class T1, class T2, class... Ts>
struct unify<T1, T2, Ts...> {
  using type = unify<unify_t<T1, T2>, Ts...>;
};

template<class T, class U>
struct pointer_cast {
  static auto fun(const U *ptr) { return (const T *)ptr; }
  static auto fun(U *ptr) { return (T *)ptr; }
};

template<class T>
inline auto pointer_cast_f(const auto &ptr) {
  return pointer_cast<T, RAWTYPE(*ptr)>::fun(ptr);
}
