#pragma once

#include <cstdint>

#include <iomanip>

#include "meta.hpp"

template<class T>
struct esc {
  const T &value;
};

template<class T>
esc(T) -> esc<T>;

std::ostream &operator<<(std::ostream &os, const esc<const char *> &e);

std::ostream &operator<<(std::ostream &os, const esc<char *> &e);

std::ostream &operator<<(std::ostream &os, const esc<std::string> &e);

std::ostream &operator<<(std::ostream &os, const esc<uint16_t> &e);

std::ostream &operator<<(std::ostream &os, const esc<uint64_t> &e);

template<class T1, class T2>
inline std::ostream &operator<<(
    std::ostream &os, const esc<std::pair<T1, T2>> &e) {
  auto &p = e.value;
  return os << "(pair " << esc{p.first} << ' ' << esc{p.second} << ')';
}

template<class T>
inline std::ostream &operator<<(std::ostream &os, const esc<T *> &e) {
  auto p = e.value;
  if (p) {
    os << "(pointer " << esc{*p} << std::endl;
  } else {
    os << "nullptr";
  }
  return os;
}

template<UnsignedInteger T>
struct hex {
  const T &value;
};

template<class T>
hex(T) -> hex<T>;

template<class T>
inline std::ostream &operator<<(std::ostream &os, const hex<T> &e) {
  os << std::setfill('0');
  auto &v = e.value;
  return os << "0x" << std::setbase(16) << std::setw(sizeof(v) * 2) << v
            << std::setbase(10);
}
