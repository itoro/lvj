#include "esc.hpp"

std::ostream &operator<<(std::ostream &os, const esc<const char *> &e) {
  auto &value = e.value;
  if (value) {
    os << '\"';
    for (auto i = size_t{0U}; value[i]; i++) {
      auto &c = value[i];
      if (c == '\"' || c == '\\') {
        os << '\\';
      }
      os << c;
    }
    os << '\"';
  } else {
    os << "nullptr";
  }
  return os;
}

std::ostream &operator<<(std::ostream &os, const esc<char *> &e) {
  return os << esc{(const char *)e.value};
}

std::ostream &operator<<(std::ostream &os, const esc<std::string> &e) {
  return os << esc{e.value.c_str()};
}

std::ostream &operator<<(std::ostream &os, const esc<uint16_t> &e) {
  return os << e.value;
}

std::ostream &operator<<(std::ostream &os, const esc<uint64_t> &e) {
  return os << e.value;
}
