#pragma once

#include <map>

#include <linux/uinput.h>

#include "float.hpp"
#include "utils.hpp"

using type_t = RAWTYPE(std::declval<input_event>().type);

using code_t = RAWTYPE(std::declval<input_event>().code);

using value_t = RAWTYPE(std::declval<input_event>().value);

static_assert(std::is_same_v<type_t, uint16_t>);

static_assert(std::is_same_v<code_t, uint16_t>);

static_assert(std::is_same_v<value_t, int32_t>);

template<class Tin>
struct table_t {
  std::vector<Tin> x;
  std::vector<flt_t> y;
};

using input_table_t = table_t<value_t>;

struct input_t {
  option_t<std::set<code_t>> keys;
  std::map<code_t, input_table_t> abss;
  bool keep_other_abss = true;
};

using mapping_table_t = table_t<flt_t>;

struct abs_mapping_t {
  option_t<code_t> code;
  option_t<mapping_table_t> table;
};

struct key_to_abs_mapping_t {
  code_t code;
  flt_t value;
};

struct abs_to_key_zone_t {
  option_t<code_t> code;
  flt_t left;
  flt_t right;
};

using abs_to_keys_t = std::vector<abs_to_key_zone_t>;

struct mapping_t {
  std::map<code_t, option_t<code_t>> keys;
  bool keep_other_keys;
  std::map<code_t, abs_mapping_t> abss;
  bool keep_other_abss;
  std::map<code_t, key_to_abs_mapping_t> keys_to_abss;
  std::map<code_t, abs_to_keys_t> abss_to_keys;
};

struct virtual_mapping_t {
  size_t controller;
  mapping_t mapping;
};

struct abs_output_t {
  value_t min;
  value_t max;
  flt_t zero;
  flt_t unit;
};

struct output_t {
  std::string name;
  input_id id;
  std::set<code_t> keys;
  std::map<code_t, option_t<abs_output_t>> abss;
};

struct ff_mapping_t {
  size_t input_controller;
  size_t output_controller;
  bool clamp_duration = true;
};

struct config_t {
  std::string name;
  std::vector<input_t> inputs;
  std::vector<output_t> outputs;
  std::map<size_t, std::vector<virtual_mapping_t>> mappings;
  std::map<size_t, size_t> output_mappings;
  std::vector<ff_mapping_t> ff_mappings;
};
