SHELL=sh

.PHONY: default

default: all

include .config.mk

CPPSTD=-std=c++20 -pedantic
CPPDWS=-Wno-parentheses -Wno-missing-field-initializers
CPPWARGS=-Wall -Wextra -Wconversion $(CPPDWS)
CPPWERROR=-Werror -Wfatal-errors
CPPOPT=-O3
CPP=$(CPPCMD) $(CPPSTD) $(CPPDEF) $(CPPWARGS) $(CPPWERROR) $(CPPOPT)
LNK=$(CPPCMD)

RM=rm -f

EXE=lvj

include .files.mk

OBJS=$(CPPFILES:.cpp=.o)

.PHONY: all clean

all: $(EXE)

clean:
	$(RM) $(OBJS) $(EXE)

$(EXE): $(OBJS)
	$(LNK) -o $(EXE) $(OBJS)

.PHONY: depend files-file depend-file

depend:
	$(MAKE) files-file
	$(MAKE) depend-file

files-file:
	( \
	  echo "HPPFILES=$$(echo *.hpp)" && \
	  echo && \
	  echo "CPPFILES=$$(echo *.cpp)" \
	) >.files.mk

depend-file: $(HPPFILES) $(CPPFILES)
	$(CPP) -M $(CPPFILES) >.depend.mk

.SECONDARY:
.SUFFIXES:
.SUFFIXES: .cpp .o

.cpp.o:
	$(CPP) -c $<

include .depend.mk
