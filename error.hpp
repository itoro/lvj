#pragma once

#include <sstream>

struct pos_t {
  const char *file;
  int line;
};

#define POS (pos_t{.file = __FILE__, .line = __LINE__})

struct err_t {
  pos_t pos;
  std::string message;
};

template<class T>
inline T fail(const auto &fpos, const auto &msg) {
  throw err_t{.pos = fpos, .message = msg};
}

template<class T>
inline auto fail_with(const auto &fpos, const auto &f) {
  auto os = std::ostringstream{};
  f(os);
  return fail<T>(fpos, os.str());
}

inline auto fail_with(const auto &fpos, const auto &f) {
  fail_with<void>(fpos, f);
}

template<class T>
inline auto not_yet(const auto &fpos) {
  return fail<T>(fpos, "not implemented");
}

inline auto not_yet(const auto &fpos) { not_yet<void>(fpos); }

inline auto check_with(
    const auto &fpos, const auto &cond, const auto &f) {
  if (!cond) {
    fail_with<void>(fpos, f);
  }
}

inline auto check(const auto &fpos, const auto &cond, const auto &msg) {
  check_with(fpos, cond, [&](auto &os) { os << msg; });
}

inline auto assert(const auto &fpos, const auto &cond) {
  check(fpos, cond, "assertion failed");
}

template<class T>
inline auto never(const auto &fpos) {
  return fail<T>(fpos, "unreachable code reached");
}

inline auto never(const auto &fpos) { never<void>(fpos); }
