#pragma once

#include <initializer_list>
#include <type_traits>
#include <utility>

#include "meta.hpp"

// todo: maybe try to merge the two classes into one. Maybe...

template<class T, auto Ref = false>
class option_t;

template<class T>
class option_t<T, false> {
 public:

  option_t() : _has_value{false} {}

  option_t(const T &value) : _has_value{true}, _value{value} {}

  option_t(std::initializer_list<T> items) {
    auto it = items.begin();
    if (it == items.end()) {
      this->_has_value = false;
    } else {
      this->_has_value = true;
      this->_value = *it;
    }
  }

  void iter(const auto &fnoval, const auto &fval) const {
    if (this->_has_value) {
      fval(this->_value);
    } else {
      fnoval();
    }
  }

  void iter(const auto &fval) const {
    this->iter([]() {}, fval);
  }

  auto fold(const auto &fnoval, const auto &fval) const
      -> unify_t<
          std::invoke_result_t<decltype(fnoval)>,
          std::invoke_result_t<decltype(fval), const T &>> {
    if (this->_has_value) {
      return fval(this->_value);
    } else {
      return fnoval();
    }
  }

  auto any(const auto &f) const {
    return this->fold(
        []() { return false; }, [&](const auto &x) { return f(x); });
  }

  auto all(const auto &f) const {
    return this->fold(
        []() { return true; }, [&](const auto &x) { return f(x); });
  }

  auto any() const {
    return this->any([](const auto &) { return true; });
  }

  auto all() const {
    return this->all([](const auto &) { return false; });
  }

  struct const_iterator {
    const_iterator() : _ptr{nullptr} {}
    const_iterator(const T *ptr) : _ptr{ptr} {}
    const auto &operator*() const { return *this->_ptr; }
    auto operator!=(const const_iterator &other) const {
      return this->_ptr != other._ptr;
    }
    auto &operator++() {
      this->_ptr = nullptr;
      return *this;
    }
    const T *_ptr;
  };

  auto begin() const {
    return this->fold(
        [&]() { return this->end(); },
        [](const auto &value) { return const_iterator{&value}; });
  }

  auto end() const { return const_iterator{}; }

 private:
  bool _has_value;
  T _value;
};

template<class T>
option_t(T) -> option_t<T>;

template<class T>
class option_t<T, true> {
 public:

  option_t() : _value{nullptr} {}

  option_t(T &value) : _value{&value} {}

  void iter(const auto &fnoval, const auto &fval) const {
    if (this->_value) {
      fval(*this->_value);
    } else {
      fnoval();
    }
  }

  void iter(const auto &fval) const {
    this->iter([]() {}, fval);
  }

  auto fold(const auto &fnoval, const auto &fval) const
      -> unify_t<
          std::invoke_result_t<decltype(fnoval)>,
          std::invoke_result_t<decltype(fval), const T &>> {
    if (this->_value) {
      return fval(*this->_value);
    } else {
      return fnoval();
    }
  }

  auto any(const auto &f) const {
    return this->fold(
        []() { return false; }, [&](const auto &x) { return f(x); });
  }

  auto all(const auto &f) const {
    return this->fold(
        []() { return true; }, [&](const auto &x) { return f(x); });
  }

  auto any() const {
    return this->any([](const auto &) { return true; });
  }

  auto all() const {
    return this->all([](const auto &) { return false; });
  }

  struct const_iterator {
    const_iterator() : _ptr{nullptr} {}
    const_iterator(const T *ptr) : _ptr{ptr} {}
    const auto &operator*() const { return *this->_ptr; }
    auto operator!=(const const_iterator &other) const {
      return this->_ptr != other._ptr;
    }
    auto &operator++() {
      this->_ptr = nullptr;
      return *this;
    }
    const T *_ptr;
  };

  auto begin() const {
    return this->fold(
        [&]() { return this->end(); },
        [](const auto &value) { return const_iterator{&value}; });
  }

  auto end() const { return const_iterator{}; }

 private:
  T *_value;
};
