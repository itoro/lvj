#pragma once

#include <map>
#include <set>
#include <vector>

#include "esc.hpp"
#include "option.hpp"

template<class T>
struct type_name;

template<class T>
inline std::ostream &type_name_f(std::ostream &os) {
  type_name<T>::f(os);
  return os;
}

template<>
struct type_name<int> {
  static auto f(auto &os) { os << "int"; }
};

template<>
struct type_name<size_t> {
  static auto f(auto &os) { os << "size_t"; }
};

template<class T>
inline auto parse(const auto &fpos, const auto &str) {
  auto iss = std::istringstream{str};
  auto ret = T{};
  iss >> ret;
  check_with(fpos, !iss.fail() && iss.eof(), [&](auto &os) {
    os << esc{str} << " is not an " << type_name_f<T>;
  });
  return ret;
}

template<class T, auto Begin, auto End>
struct range_set {
  static auto _f(auto &accu) {
    if constexpr (Begin < End) {
      accu.insert(Begin);
      range_set<T, Begin + 1, End>::_f(accu);
    }
  }
  static auto f() {
    auto r = std::set<T>{};
    range_set<T, Begin, End>::_f(r);
    return r;
  }
};

inline auto any(auto &&seq, const auto &p) {
  for (auto &&x : seq) {
    if (p(FWD(x))) {
      return true;
    }
  }
  return false;
}

inline auto iter(auto &&seq, const auto &f) {
  for (auto &&x : seq) {
    f(FWD(x));
  }
}

template<class T>
struct iteri_helper;

template<class T, class Allocator>
struct iteri_helper<std::vector<T, Allocator>> {
  static auto f(auto &&seq, const auto &ff) {
    auto sz = seq.size();
    for (auto i = AS(sz, 0); i < sz; i++) {
      ff(i, FWD(seq[i]));
    }
  }
};

template<class Key, class T, class Compare, class Allocator>
struct iteri_helper<std::map<Key, T, Compare, Allocator>> {
  static auto f(auto &&seq, const auto &ff) {
    for (auto &&[k, v] : seq) {
      ff(FWD(k), FWD(v));
    }
  }
};

inline auto iteri(auto &&seq, const auto &f) {
  iteri_helper<RAWTYPE(seq)>::f(FWD(seq), f);
}

// todo: implement a filter iterator
inline auto bind(auto &&seq, const auto &f) {
  auto res = std::vector<RAWTYPE(*f(*seq.begin()).begin())>{};
  for (auto &&x : seq) {
    auto &&y = f(FWD(x));
    for (auto &&z : y) {
      res.push_back(FWD(z));
    }
  }
  return res;
}

inline auto fold(auto &&accu, auto &&seq, const auto &f) {
  // todo: kof kof kof???
  for (auto &&x : seq) {
    accu = f(FWD(accu), FWD(x));
  }
  return accu;
}

inline auto zero(auto &data) { memset(&data, 0, sizeof(data)); }

template<class T>
inline auto operator==(const option_t<T> &x1, const option_t<T> &x2) {
  return x1.fold(
      [&]() { return x2.all(); },
      [&](const auto &x1) {
        return x2.fold(
            []() { return false; },
            [&](const auto &x2) { return x1 == x2; });
      });
}

inline auto to_unsigned(const auto &n) {
  return (std::make_unsigned_t<RAWTYPE(n)>)n;
}

inline auto to_signed(const auto &u) {
  return (std::make_signed_t<RAWTYPE(u)>)u;
}

inline auto as_unsigned(const auto &fpos, const auto &n) {
  check(fpos, n >= 0, "negative value");
  return to_unsigned(n);
}

inline auto as_signed(const auto &fpos, const auto &n) {
  using T = std::make_signed_t<RAWTYPE(n)>;
  check(fpos, n <= std::numeric_limits<T>::max(), "too large value");
  return to_signed(n);
}

template<Integer T>
inline auto safe_cast(const auto &fpos, const auto &x) {
  check(fpos, x >= std::numeric_limits<T>::min(), "too small value");
  check(fpos, x <= std::numeric_limits<T>::max(), "too big value");
  return (T)x;
}

template<class T>
using pointer_t = T *;

template<UnsignedInteger T>
inline auto rotate_bits(const T &u) {
  auto res = CAST(u, u << 1);
  if (res < u) {
    res++;
  }
  return res;
}

template<class T1, class T2>
struct std::hash<std::pair<T1, T2>> {
  auto operator()(const auto &p) const {
    auto h1 = std::hash<T1>{};
    auto h2 = std::hash<T2>{};
    return rotate_bits(h1(p.first)) ^ h2(p.second);
  }
};

template<class... Ts>
struct auto_hasher;

template<>
struct auto_hasher<> {
  static auto f(const auto &accu) { return accu; }
};

template<class T, class... Ts>
struct auto_hasher<T, Ts...> {
  static auto f(auto accu, const T &arg, Ts... args) {
    auto h = std::hash<T>{};
    accu = rotate_bits(accu) ^ h(arg);
    return auto_hasher<Ts...>::f(accu, args...);
  }
};

template<class... Ts>
inline auto auto_hasher_f(Ts... args) {
  return auto_hasher<Ts...>::f(size_t{0}, args...);
}
