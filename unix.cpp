#include "unix.hpp"

#include <fcntl.h>
#include <unistd.h>

#include "debug.hpp"
#include "utils.hpp"

void s_close(int fd) {
  fdebug(1, [&](auto &dbg) { dbg << "close " << fd; });
  check(POS, close(fd) == 0, "close failed");
}

const int fd_helper_t::invalid = -1;

bool fd_helper_t::is_valid(int fd) { return fd >= 0; }

void fd_helper_t::reset(int fd) { s_close(fd); }

fd_t s_open(const char *pathname, int flags) {
  auto ifd = open(pathname, flags);
  check_with(POS, ifd >= 0, [&](auto &os) {
    os << "open " << esc{pathname} << " failed";
  });
  auto fd = fd_t{ifd};
  fdebug(1, [&](auto &dbg) {
    dbg << "open " << ifd << ' ' << esc{pathname};
  });
  return fd;
}

size_t s_write(int fd, const void *buf, size_t count) {
  auto w = write(fd, buf, count);
  check(POS, w > 0, "write failed");
  return to_unsigned(w);
}

int s_poll(struct pollfd *fds, nfds_t nfds, int timeout) {
  auto st = poll(fds, nfds, timeout);
  check(POS, st >= 0, "poll failed");
  return st;
}

size_t s_read(int fd, void *buf, size_t count) {
  auto r = read(fd, buf, count);
  check(POS, r >= 0, "read failed");
  return to_unsigned(r);
}
